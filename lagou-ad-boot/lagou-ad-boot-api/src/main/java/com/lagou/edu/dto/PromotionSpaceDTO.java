package com.lagou.edu.dto;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

public class PromotionSpaceDTO implements Serializable {

    private Integer id;

    private String name;

    private String spaceKey;

    private LocalDateTime createTime;

    private LocalDateTime updateTime;

    private Integer isDel;

    private List<PromotionAdDTO> promotionAdDTOList;

    public List<PromotionAdDTO> getPromotionAdDTOList() {
        return promotionAdDTOList;
    }

    public void setPromotionAdDTOList(List<PromotionAdDTO> promotionAdDTOList) {
        this.promotionAdDTOList = promotionAdDTOList;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSpaceKey() {
        return spaceKey;
    }

    public void setSpaceKey(String spaceKey) {
        this.spaceKey = spaceKey;
    }

    public LocalDateTime getCreateTime() {
        return createTime;
    }

    public void setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
    }

    public LocalDateTime getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(LocalDateTime updateTime) {
        this.updateTime = updateTime;
    }

    public Integer getIsDel() {
        return isDel;
    }

    public void setIsDel(Integer isDel) {
        this.isDel = isDel;
    }
}
