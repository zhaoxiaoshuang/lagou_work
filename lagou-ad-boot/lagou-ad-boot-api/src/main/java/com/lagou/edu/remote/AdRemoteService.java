package com.lagou.edu.remote;

import com.lagou.edu.dto.PromotionAdDTO;
import com.lagou.edu.dto.PromotionSpaceDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;

/**
 * @author zhaozheng
 */
@FeignClient(name = "lagou-edu-ad",path = "/ad")
public interface AdRemoteService {
    @GetMapping("/space/getAllSpaces")
    List<PromotionSpaceDTO> getAllSpaces();


    @GetMapping(value = "/getAdBySpaceKey")
    List<PromotionSpaceDTO> getAdBySpaceKey(@RequestParam("spaceKey")String[] spaceKey);

    @PostMapping(value = "/space/saveOrUpdateSpace")
    public Integer addSpace(@RequestBody PromotionSpaceDTO spaceDTO);

    @GetMapping(value = "/space/getSpaceById")
    PromotionSpaceDTO getSpaceById(@RequestParam("id")Integer id);
    @GetMapping(value = "/space/getAllAds")
    List<PromotionAdDTO> getALLAds();
}
