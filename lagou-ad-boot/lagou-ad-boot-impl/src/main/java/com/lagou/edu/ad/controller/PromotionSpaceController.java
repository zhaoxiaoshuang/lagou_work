package com.lagou.edu.ad.controller;


import com.lagou.edu.ad.entity.PromotionSpace;
import com.lagou.edu.ad.service.IPromotionSpaceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author felix
 * @since 2021-03-30
 */
//@RestController
@RequestMapping("/ad/space")
public class PromotionSpaceController {
    @Autowired
    private IPromotionSpaceService spaceService;

    @GetMapping(value = "/getAllSpaces")
    public List<PromotionSpace> getAllSpace() {
        List<PromotionSpace> spaceList = spaceService.list();
        return spaceList;
    }

}
