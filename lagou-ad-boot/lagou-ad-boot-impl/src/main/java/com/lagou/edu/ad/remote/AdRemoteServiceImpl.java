package com.lagou.edu.ad.remote;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.lagou.edu.ad.entity.PromotionAd;
import com.lagou.edu.ad.entity.PromotionSpace;
import com.lagou.edu.ad.service.IPromotionAdService;
import com.lagou.edu.ad.service.IPromotionSpaceService;
import com.lagou.edu.dto.PromotionAdDTO;
import com.lagou.edu.dto.PromotionSpaceDTO;
import com.lagou.edu.remote.AdRemoteService;
import com.lagou.edu.util.ConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping(value = "/ad")
public class AdRemoteServiceImpl implements AdRemoteService {
    @Autowired
    private IPromotionSpaceService spaceService;
    @Autowired
    private IPromotionAdService adService;

    @GetMapping(value = "/space/getAllSpaces")
    @Override
    public List<PromotionSpaceDTO> getAllSpaces() {
        List<PromotionSpace> list = spaceService.list();
        return ConvertUtil.Convert(list, PromotionSpaceDTO.class);
    }

    @GetMapping(value = "/getAdBySpaceKey")
    @Override
    public List<PromotionSpaceDTO> getAdBySpaceKey(String[] spaceKey) {
        List<PromotionSpaceDTO> list = new ArrayList<>();

        for (int i = 0; i < spaceKey.length; i++) {
            QueryWrapper<PromotionSpace> query = new QueryWrapper<>();
            String key = spaceKey[i];
            query.eq("spaceKey", key);
            PromotionSpace promotionSpace = spaceService.getOne(query);
            QueryWrapper<PromotionAd> adQueryWrapper = new QueryWrapper<>();
            adQueryWrapper.eq("spaceId", promotionSpace.getId());
            //状态为上线状态
            adQueryWrapper.eq("status", 1);
            //有效期内
//            LocalDateTime time = LocalDateTime.now();
//            adQueryWrapper.lt("startTime", time);
//            adQueryWrapper.gt("endTime", time);
            List<PromotionAd> promotionAd = adService.list(adQueryWrapper);
            List<PromotionAdDTO> promotionAdDTOList = ConvertUtil.Convert(promotionAd, PromotionAdDTO.class);
            PromotionSpaceDTO spaceDTO = ConvertUtil.Convert(promotionSpace, PromotionSpaceDTO.class);
            spaceDTO.setPromotionAdDTOList(promotionAdDTOList);
            list.add(spaceDTO);
        }

        return list;
    }

    @PostMapping(value = "/space/saveOrUpdateSpace")
    @Override
    public Integer addSpace(@RequestBody PromotionSpaceDTO spaceDTO) {
        PromotionSpace promotionSpace = ConvertUtil.Convert(spaceDTO, PromotionSpace.class);
        boolean update = spaceService.saveOrUpdate(promotionSpace);
        return update ? 1 : 0;
    }

    @PostMapping(value = "/space/getSpaceById")
    @Override
    public PromotionSpaceDTO getSpaceById(Integer id) {
        PromotionSpace promotionSpace = spaceService.getById(id);
        if (promotionSpace != null) {
            PromotionSpaceDTO spaceDTO = ConvertUtil.Convert(promotionSpace, PromotionSpaceDTO.class);
            return spaceDTO;
        }
        return null;
    }

    @GetMapping(value = "/getALLAds")
    @Override
    public List<PromotionAdDTO> getALLAds() {
        List<PromotionAd> promotionAdList = adService.list();
        List<PromotionAdDTO> promotionAdDTOList = ConvertUtil.Convert(promotionAdList, PromotionAdDTO.class);
        return promotionAdDTOList;
    }
}
