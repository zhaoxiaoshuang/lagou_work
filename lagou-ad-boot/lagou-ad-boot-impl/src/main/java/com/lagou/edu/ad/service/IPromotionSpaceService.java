package com.lagou.edu.ad.service;

import com.lagou.edu.ad.entity.PromotionSpace;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author felix
 * @since 2021-03-30
 */
public interface IPromotionSpaceService extends IService<PromotionSpace> {

}
