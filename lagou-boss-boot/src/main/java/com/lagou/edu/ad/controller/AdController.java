package com.lagou.edu.ad.controller;

import com.lagou.edu.dto.PromotionAdDTO;
import com.lagou.edu.dto.PromotionSpaceDTO;
import com.lagou.edu.model.ResponseDTO;
import com.lagou.edu.remote.AdRemoteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/ad")
public class AdController {

    @Autowired
    private AdRemoteService adRemoteService;

    @GetMapping(value = "/space/getAllSpaces")
    public ResponseDTO getAllSpaces() {
        List<PromotionSpaceDTO> spaceDTOList = adRemoteService.getAllSpaces();

        return ResponseDTO.success(spaceDTOList);
    }


    @GetMapping(value = "/getAdBySpaceKey")
    public ResponseDTO getAdBySpaceKey(@RequestParam("spaceKey") String[] spaceKey) {
        List<PromotionSpaceDTO> spaceDTOS = adRemoteService.getAdBySpaceKey(spaceKey);

        return ResponseDTO.success(spaceDTOS);
    }

    @PostMapping(value = "/space/saveOrUpdateSpace")
    public ResponseDTO saveOrUpdateSpace(@RequestBody() PromotionSpaceDTO spaceDTO) {
        Integer integer = adRemoteService.addSpace(spaceDTO);

        return ResponseDTO.success(integer);
    }

    @GetMapping("/space/getSpaceById")
    public ResponseDTO getSpaceById(@RequestParam("id") Integer id) {
        PromotionSpaceDTO spaceDTO = adRemoteService.getSpaceById(id);
        return ResponseDTO.success(spaceDTO);
    }

    @GetMapping("/getAllAds")
    public ResponseDTO getAllAds(){
        List<PromotionAdDTO> promotionAdDTOList = adRemoteService.getALLAds();
        return ResponseDTO.success(promotionAdDTOList);
    }
}
