package com.lagou.edu.course.controller;

import com.lagou.edu.dto.CourseDTO;
import com.lagou.edu.dto.PageResultDTO;
import com.lagou.edu.message.remote.MessageRemoteService;
import com.lagou.edu.model.ResponseDTO;
import com.lagou.edu.param.CourseQueryParam;
import com.lagou.edu.remote.CourseRemoteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.LinkedHashMap;


@RestController
@RequestMapping(value = "/course")
public class CourseController {

    @Autowired
    private CourseRemoteService courseRemoteService;
    @Autowired
    private MessageRemoteService messageRemoteService;
    /**
     * 分页获取列表.
     *
     * @param courseQueryParam
     * @return
     */
    @PostMapping(value = "/getQueryCourses")
    PageResultDTO<CourseDTO> getQueryCourses(@RequestBody CourseQueryParam courseQueryParam) {
        return courseRemoteService.getQueryCourses(courseQueryParam);
    }

    /**
     * 根据course获取id
     */
    @GetMapping(value = "getCourseById")
    public ResponseDTO<CourseDTO> getCourseById(@RequestParam("courseId") Integer courseId, @RequestParam("userId") Integer userId) {
        var dto = courseRemoteService.getCourseById(courseId, userId);
        return ResponseDTO.success(dto);
    }

    /**
     * 新建或保存
     */
    @PostMapping(value = "saveOrUpdateCourse")
    public ResponseDTO saveOrUpdateCourse(@RequestBody CourseDTO courseDTO) {
        boolean b = courseRemoteService.saveOrUpdateCourse(courseDTO);
        return b ? ResponseDTO.success() : ResponseDTO.ofError("保存失败");
    }

    @PostMapping(value = "saveOrUpdateSectionByCourseId")
    public ResponseDTO saveOrUpdateSectionByCourseId(@RequestBody CourseDTO courseDTO) {
        boolean b = courseRemoteService.saveOrUpdateCourse(courseDTO);
        return b ? ResponseDTO.success() : ResponseDTO.ofError("保存失败");
    }

    @PostMapping(value = "saveOrUpdateLessonByCourseIdAndSectionId")
    public ResponseDTO saveOrUpdateLessonByCourseIdAndSectionId(@RequestBody CourseDTO courseDTO) {
        boolean b = courseRemoteService.saveOrUpdateCourse(courseDTO);
        return b ? ResponseDTO.success() : ResponseDTO.ofError("保存失败");
    }

    @PostMapping(value = "operateCourseById")
    public ResponseDTO operateCourseById(@RequestBody CourseDTO courseDTO) {
        CourseDTO param = new CourseDTO();
        var status = courseDTO.getStatus();
        var id = courseDTO.getId();
        if (id == null) {
            return ResponseDTO.ofError("id不能为空");
        }
        if (status == null) {
            return ResponseDTO.ofError("上架状态不能为空");
        }
        param.setId(id);
        param.setStatus(status);
        boolean b = courseRemoteService.saveOrUpdateCourse(param);
        LinkedHashMap<String,Object> messageParam=new LinkedHashMap<>();
        messageParam.put("msg","XXX课程已上线,请及时学习");
        if (b&&status==1)messageRemoteService.sendMessage(messageParam);
        return b ? ResponseDTO.success() : ResponseDTO.ofError("保存失败");
    }
}
