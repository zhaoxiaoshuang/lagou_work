package com.lagou.edu.key;

public interface KeyGenerator {

    Number generateKey();

}
