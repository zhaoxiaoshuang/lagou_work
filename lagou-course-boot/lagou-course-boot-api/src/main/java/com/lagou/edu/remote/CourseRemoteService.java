package com.lagou.edu.remote;

import com.lagou.edu.dto.CourseDTO;
import com.lagou.edu.dto.PageResultDTO;
import com.lagou.edu.param.CourseQueryParam;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
/**
 * @author mkp
 */
@FeignClient(name = "lagou-edu-course", path = "/course")
public interface CourseRemoteService {
    /**
     * 获取选课列表
     * @param userId
     * @return
     */
    @GetMapping("/getAllCourses")
    List<CourseDTO> getAllCourses(@RequestParam(required = false, name = "userId") Integer userId);

    /**
     * 获取已购课程信息
     * @param userId
     * @return
     */
    @GetMapping("/getPurchasedCourse")
    List<CourseDTO> getPurchasedCourse(@RequestParam("userId") Integer userId);

    /**
     * 获取课程详情
     * @param courseId
     * @return
     */
    @GetMapping("/getCourseById")
    CourseDTO getCourseById(@RequestParam("courseId") Integer courseId,@RequestParam(required = false ,name = "userId") Integer userId);


    /**
     * 更新课程,更新营销信息,更新 status,更新课时信息
     * @param courseDTO
     * @return
     */
    @PostMapping(value = "/saveOrUpdateCourse",consumes = "application/json")
    boolean saveOrUpdateCourse(@RequestBody CourseDTO courseDTO);


   @PostMapping(value = "/getQueryCourses",consumes = "application/json")
   PageResultDTO<CourseDTO> getQueryCourses(@RequestBody CourseQueryParam courseQueryParam);

}
