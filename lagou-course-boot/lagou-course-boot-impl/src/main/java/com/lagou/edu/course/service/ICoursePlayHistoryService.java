package com.lagou.edu.course.service;

import com.lagou.edu.course.entity.CoursePlayHistory;
import com.baomidou.mybatisplus.extension.service.IService;
import com.lagou.edu.dto.CoursePlayHistoryDTO;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author leo
 * @since 2020-06-19
 */
public interface ICoursePlayHistoryService extends IService<CoursePlayHistory> {

    CoursePlayHistory getByUserIdAndCourseId(Integer userId, Integer courseId);

    void saveCourseHistoryNode(CoursePlayHistoryDTO playHistoryDTO);
}
