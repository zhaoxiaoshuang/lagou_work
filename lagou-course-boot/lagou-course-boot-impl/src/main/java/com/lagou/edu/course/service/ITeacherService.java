package com.lagou.edu.course.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lagou.edu.course.entity.Teacher;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author leo
 * @since 2020-06-11
 */
public interface ITeacherService extends IService<Teacher> {

}
