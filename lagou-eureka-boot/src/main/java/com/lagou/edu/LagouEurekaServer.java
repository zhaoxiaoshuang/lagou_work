package com.lagou.edu;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;


@SpringBootApplication
@EnableEurekaServer
public class LagouEurekaServer {
    public static void main(String[] args) {
        SpringApplication.run(LagouEurekaServer.class, args);
    }
}
