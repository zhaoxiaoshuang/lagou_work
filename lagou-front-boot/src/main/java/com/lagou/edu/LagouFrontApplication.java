package com.lagou.edu;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients(value = "com.lagou.edu")
@EnableDiscoveryClient
public class LagouFrontApplication {
    public static void main(String[] args) {
        SpringApplication.run(LagouFrontApplication.class,args);
    }
}
