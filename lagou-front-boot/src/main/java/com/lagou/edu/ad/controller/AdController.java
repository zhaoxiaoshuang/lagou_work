package com.lagou.edu.ad.controller;

import com.lagou.edu.dto.PromotionSpaceDTO;
import com.lagou.edu.model.ResponseDTO;
import com.lagou.edu.remote.AdRemoteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/ad")
public class AdController {
    @Autowired
    private AdRemoteService remoteService;
    @GetMapping(value = "/getAdBySpaceKey")
    public ResponseDTO getAdBySpaceKey(@RequestParam(name = "spaceKey") String[] spaceKey){
        List<PromotionSpaceDTO> adBySpaceKey = remoteService.getAdBySpaceKey(spaceKey);
        return ResponseDTO.success(adBySpaceKey);
    }
}
