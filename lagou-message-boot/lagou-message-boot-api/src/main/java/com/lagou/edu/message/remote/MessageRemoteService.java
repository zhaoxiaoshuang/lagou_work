package com.lagou.edu.message.remote;

import java.util.LinkedHashMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient(value = "lagou-edu-message", path = "/message")
public interface MessageRemoteService {

	@PostMapping(value = "/send")
	public void sendMessage(@RequestBody LinkedHashMap<String, Object> param);

}
