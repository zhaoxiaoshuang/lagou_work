package com.lagou.edu.message.remote;

import java.util.LinkedHashMap;

import org.springframework.beans.factory.annotation.Autowired;

import com.lagou.edu.message.netty.NettyServer;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MessageRemoteServiceImpl implements MessageRemoteService {

	@Autowired
	private NettyServer nettyServer;
	@PostMapping(value = "/message/send")
	@Override
	public void sendMessage(@RequestBody LinkedHashMap<String, Object> param) {
		nettyServer.send(param.get("msg").toString());

	}

}
