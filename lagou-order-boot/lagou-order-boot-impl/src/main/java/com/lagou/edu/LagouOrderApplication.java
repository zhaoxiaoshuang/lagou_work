package com.lagou.edu;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
@MapperScan("com.lagou.edu.order.mapper")
public class LagouOrderApplication {
    public static void main(String[] args) {
        SpringApplication.run(LagouOrderApplication.class, args);
    }
}
