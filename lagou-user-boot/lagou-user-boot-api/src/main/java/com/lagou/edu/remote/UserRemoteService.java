package com.lagou.edu.remote;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.lagou.edu.dto.UserDTO;
import com.lagou.edu.param.UserQueryParam;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * @author zhaozheng
 */
@FeignClient(name = "lagou-edu-user", path = "/user")
public interface UserRemoteService {
    @PostMapping(value = "/getUserPages")
    Page<UserDTO> getUserPages(@RequestBody UserQueryParam param);

    UserDTO getUserByPhone(String username);

    UserDTO getUserById(Integer userId);

    UserDTO saveUser(UserDTO dto);
}
