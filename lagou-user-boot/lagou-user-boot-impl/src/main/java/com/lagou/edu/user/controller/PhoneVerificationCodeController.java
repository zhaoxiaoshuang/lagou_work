package com.lagou.edu.user.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author tony.zheng
 * @since 2021-04-07
 */
@RestController
@RequestMapping("/user/phone-verification-code")
public class PhoneVerificationCodeController {

}
