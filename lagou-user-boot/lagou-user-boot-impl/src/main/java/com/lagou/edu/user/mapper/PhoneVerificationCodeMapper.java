package com.lagou.edu.user.mapper;

import com.lagou.edu.user.entity.PhoneVerificationCode;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author tony.zheng
 * @since 2021-04-07
 */
public interface PhoneVerificationCodeMapper extends BaseMapper<PhoneVerificationCode> {

}
