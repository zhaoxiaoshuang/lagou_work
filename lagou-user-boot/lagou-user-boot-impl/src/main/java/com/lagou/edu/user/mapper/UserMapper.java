package com.lagou.edu.user.mapper;

import com.lagou.edu.user.entity.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author tony.zheng
 * @since 2021-04-07
 */
public interface UserMapper extends BaseMapper<User> {

}
