package com.lagou.edu.user.remote;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.lagou.edu.dto.UserDTO;
import com.lagou.edu.param.UserQueryParam;
import com.lagou.edu.remote.UserRemoteService;
import com.lagou.edu.user.entity.User;
import com.lagou.edu.user.service.IUserService;
import com.lagou.edu.util.ConvertUtil;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping(value = "/user")
public class UserRemoteServiceImpl implements UserRemoteService {
    @Autowired
    private IUserService userService;
    @PostMapping(value = "/getUserPages")
    @Override
    public Page<UserDTO> getUserPages(@RequestBody UserQueryParam param) {
        String phone = param.getPhone();
        Integer userId = param.getUserId();
        Integer currentPage = param.getCurrentPage();
        Integer pageSize = param.getPageSize();
        Date startCreateTime = param.getStartCreateTime();
        Date endCreateTime = param.getEndCreateTime();

        Page<User> page = new Page<>(currentPage, pageSize);
        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
        //根据电话号码查询用户
        if (StringUtils.isNotBlank(phone)) {
            queryWrapper.like("phone", phone);
        }
        if (null != startCreateTime && null != endCreateTime) {
            queryWrapper.ge("create_time", startCreateTime);
            queryWrapper.le("create_time", endCreateTime);
        }
        if (null != userId && userId > 0) {
            queryWrapper.eq("id", userId);
        }

        int count = userService.count(queryWrapper);
        queryWrapper.orderByDesc("id");
        IPage<User> selectPage = this.userService.getBaseMapper().selectPage(page, queryWrapper);

        List<UserDTO> userDTOList = new ArrayList<>();
        //获取课程对应的模块的信息
        for (User user : selectPage.getRecords()) {
            UserDTO userDTO = ConvertUtil.Convert(user,UserDTO.class);
            userDTOList.add(userDTO);
        }

        Page<UserDTO> result = new Page<>();
        //分页查询结果对象属性的拷贝
        ConvertUtil.Convert(selectPage, result);
        //设置分页结果对象record属性
        result.setRecords(userDTOList);
        result.setTotal(count);
        return result;
    }

    @Override
    public UserDTO getUserByPhone(String username) {
        return null;
    }

    @Override
    public UserDTO getUserById(Integer userId) {
        return null;
    }

    @Override
    public UserDTO saveUser(UserDTO dto) {
        return null;
    }
}
