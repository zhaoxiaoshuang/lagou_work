package com.lagou.edu.user.service;

import com.lagou.edu.user.entity.PhoneVerificationCode;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author tony.zheng
 * @since 2021-04-07
 */
public interface IPhoneVerificationCodeService extends IService<PhoneVerificationCode> {

}
