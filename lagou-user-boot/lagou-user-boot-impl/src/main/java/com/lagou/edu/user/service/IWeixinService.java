package com.lagou.edu.user.service;

import com.lagou.edu.user.entity.Weixin;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author tony.zheng
 * @since 2021-04-07
 */
public interface IWeixinService extends IService<Weixin> {

}
