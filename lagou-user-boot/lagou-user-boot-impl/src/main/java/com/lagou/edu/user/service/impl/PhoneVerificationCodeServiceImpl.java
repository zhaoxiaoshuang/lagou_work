package com.lagou.edu.user.service.impl;

import com.lagou.edu.user.entity.PhoneVerificationCode;
import com.lagou.edu.user.mapper.PhoneVerificationCodeMapper;
import com.lagou.edu.user.service.IPhoneVerificationCodeService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author tony.zheng
 * @since 2021-04-07
 */
@Service
public class PhoneVerificationCodeServiceImpl extends ServiceImpl<PhoneVerificationCodeMapper, PhoneVerificationCode> implements IPhoneVerificationCodeService {

}
