package com.lagou.edu.user.service.impl;

import com.lagou.edu.user.entity.Weixin;
import com.lagou.edu.user.mapper.WeixinMapper;
import com.lagou.edu.user.service.IWeixinService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author tony.zheng
 * @since 2021-04-07
 */
@Service
public class WeixinServiceImpl extends ServiceImpl<WeixinMapper, Weixin> implements IWeixinService {

}
